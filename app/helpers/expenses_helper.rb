module ExpensesHelper
	def calculate_totals
	    @expenses = Expense.all
	    @total_processing_cost_domestic = @expenses.sum(:domestic_value)
	    @total_processing_cost_currency = @expenses.sum(:currency_value)
	end
end
