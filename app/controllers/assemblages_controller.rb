class AssemblagesController < ApplicationController
  before_action :set_assemblage, only: [:show, :edit, :update, :destroy]

  # GET /assemblages
  # GET /assemblages.json
  def index
    @assemblages = Assemblage.all
  end

  # GET /assemblages/1
  # GET /assemblages/1.json
  def show
    @assemblage = Assemblage.find(params[:id])
  end

  # GET /assemblages/new
  def new
    @assemblage = Assemblage.new
  end

  # GET /assemblages/1/edit
  def edit   
    @assemblage = Assemblage.find(params[:id])
    @subassemblages = Subassemblage.where("assemblage_id = ?", @assemblage.id)
  end

  def price_calculator
    @assemblage = Assemblage.find(params[:assemblage_id])
    @expenses = Expense.all
    @total_processing_cost_domestic = @expenses.sum(:domestic_value)
    @total_processing_cost_currency = @expenses.sum(:currency_value)

    @material = Material.find_by_id(@assemblage.material_id)
    @cost_material_prim_domestic = @material.price * @assemblage.brut_volume

    @processing_cost_domestic = @total_processing_cost_domestic * @assemblage.brut_volume
    @processing_cost_currency = @total_processing_cost_currency * @assemblage.brut_volume

    @total_cost_estimated_domestic = @cost_material_prim_domestic + @processing_cost_domestic

  end


  # POST /assemblages
  # POST /assemblages.json
  def create
    @assemblage = Assemblage.new(assemblage_params)

   if @assemblage.save
      flash[:success] = "Ansamblul a fost adaugat cu success!"
      redirect_to @assemblage
    else
      flash[:danger] = "Eroare!!"
      render 'new'
    end
  end

  # PATCH/PUT /assemblages/1
  # PATCH/PUT /assemblages/1.json
  def update
     
      if @assemblage.update(assemblage_params)
        flash[:success] = "Ansamblul a fost actualizat cu success!"
        redirect_to edit_assemblage_path
      else
        flash[:danger] = "Eroare!!"
        render 'new'
      
      end
    
  end

  # DELETE /assemblages/1
  # DELETE /assemblages/1.json
  def destroy
    @assemblage.destroy
    respond_to do |format|
      format.html { redirect_to assemblages_url, notice: 'Assemblage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assemblage
      @assemblage = Assemblage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assemblage_params
      params.require(:assemblage).permit(:name,:code,:material_id,:finish_id,:accesory_id,:accesory_name,:height,:width,:depth,:net_volume,:net_surface,:brut_volume,:brut_surface)
    end

     

end
