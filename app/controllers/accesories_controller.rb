class AccesoriesController < ApplicationController
  before_action :set_accesory, only: [:show, :edit, :update, :destroy]

  autocomplete :accesory, :name, :display_value => :accesory_name

  # GET /accesories
  # GET /accesories.json
  def index
    @accesories = Accesory.all
  end

  # GET /accesories/1
  # GET /accesories/1.json
  def show
  end

  # GET /accesories/new
  def new
    @accesory = Accesory.new
  end

  # GET /accesories/1/edit
  def edit
  end

  # POST /accesories
  # POST /accesories.json
  def create
    @accesory = Accesory.new(accesory_params)

    respond_to do |format|
      if @accesory.save
        format.html { redirect_to @accesory, notice: 'Accesory was successfully created.' }
        format.json { render :show, status: :created, location: @accesory }
      else
        format.html { render :new }
        format.json { render json: @accesory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accesories/1
  # PATCH/PUT /accesories/1.json
  def update
    respond_to do |format|
      if @accesory.update(accesory_params)
        format.html { redirect_to @accesory, notice: 'Accesory was successfully updated.' }
        format.json { render :show, status: :ok, location: @accesory }
      else
        format.html { render :edit }
        format.json { render json: @accesory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accesories/1
  # DELETE /accesories/1.json
  def destroy
    @accesory.destroy
    respond_to do |format|
      format.html { redirect_to accesories_url, notice: 'Accesory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_accesory
      @accesory = Accesory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def accesory_params
      params.require(:accesory).permit(:code, :name, :um, :price, :currency_code)
    end
end
