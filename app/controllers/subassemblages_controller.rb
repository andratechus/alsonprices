class SubassemblagesController < ApplicationController
  before_action :set_subassemblage, only: [:show, :edit, :update, :destroy]
  
  # GET /subassemblages
  # GET /subassemblages.json
  def index
    @subassemblages = Subassemblage.all
  end

  # GET /subassemblages/1
  # GET /subassemblages/1.json
  def show

  end

  # GET /subassemblages/new
  def new
    @subassemblage = Subassemblage.new
  end

  # GET /subassemblages/1/edit
  def edit
    @subassemblage = Subassemblage.find(params[:id])  
    @elements = Element.where("subassemblage_id = ?", @subassemblage.id)   
  end

  # POST /subassemblages
  # POST /subassemblages.json
  def create
    
    @subassemblage = Subassemblage.create(subassemblage_params)
   
    if @subassemblage.save
      flash[:success] = "Subansamblul a fost adaugat cu success!"
      redirect_to @subassemblage
      set_assemblage.save
    else
      flash[:success] = "Eroare!!"
      render 'new'
    end
  end

  # PATCH/PUT /subassemblages/1
  # PATCH/PUT /subassemblages/1.json
  def update  
      if @subassemblage.update(subassemblage_params)
        flash[:success] = "Subansamblul a fost adaugat cu success!"
        redirect_to @subassemblage
        set_assemblage.save
      else
        flash[:success] = "Eroare!!"
        render 'edit'
      end
  end

  # DELETE /subassemblages/1
  # DELETE /subassemblages/1.json
  def destroy
    elements_list.delete_all
    @subassemblage.destroy

    respond_to do |format|
      format.html { redirect_to subassemblages_url, notice: 'Subassemblage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subassemblage
      @subassemblage = Subassemblage.find(params[:id])
    end

   # Never trust parameters from the scary internet, only allow the white list through.
    def subassemblage_params
       params.require(:subassemblage).permit(:name,:code,:material_id,:height,:width,:depth, :quantity, :assemblage_id)
    end

    def set_assemblage
      if @subassemblage.persisted?
        @assemblage = Assemblage.find(@subassemblage.assemblage_id)
      else  
        @assemblage = Assemblage.find(params[:assemblage_id])
      end
    end
end
