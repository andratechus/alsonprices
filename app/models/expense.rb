class Expense < ActiveRecord::Base
	has_one :currency

	before_save :set_currency_code, :set_currency_value

	def currency
		currency = Currency.find_by_id(self.currency_id)
	end

	def set_currency_code
		self.currency_code = currency.code
	end

	def set_currency_value
		self.currency_value = self.domestic_value / currency.rate
	end

	
end
