class Element < ActiveRecord::Base
	belongs_to :subassemblage

	before_save :set_volume, :set_surface

	def set_volume
		self.volume = self.height * self.width * self.depth * self.quantity / 100000000
	end

	def set_surface
		self.surface = self.height * self.width * self.quantity / 1000000
	end

end
