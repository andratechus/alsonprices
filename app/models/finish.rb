class Finish < ActiveRecord::Base
	has_one :currency

	before_save :set_currency_code, :set_domestic_price
	def currency
		currency = Currency.find_by_id(self.currency_id)
	end

	def set_currency_code
		self.currency_code = currency.code
	end

	def set_domestic_price
		self.domestic_price = self.price * currency.rate
	end

end
