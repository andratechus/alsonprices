class Assemblage < ActiveRecord::Base
	has_one :material
    has_one :accesory
    has_one :finish
	has_many :subassemblage

	before_save :set_net_volume, :set_brut_volume, :set_net_surface, :set_brut_surface


    def material
        material = Material.find_by_id(self.material_id)  
    end

    def subassemblages_list
    	@subassemblages = Subassemblage.where("assemblage_id = ?", self.id)
    end

	def set_net_volume
		self.net_volume = BigDecimal.new("0.000")
		
		subassemblages_list.each do |s|
			self.net_volume = BigDecimal.new(self.net_volume) + BigDecimal.new(s.volume)
		end	
	end

    def set_brut_volume  	
        self.brut_volume = BigDecimal.new(self.net_volume / material.output_coefficient)
    end

    def set_net_surface
    	self.net_surface = BigDecimal.new("0.000")
        subassemblages_list.each do |s|
			self.net_surface = self.net_surface + s.surface
		end	
    end

    def set_brut_surface
        self.brut_surface = self.net_surface * 2

    end


end
