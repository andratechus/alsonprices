class Material < ActiveRecord::Base
	validates :name, presence: true, length: {maximum: 50}
	belongs_to :assemblage
end
