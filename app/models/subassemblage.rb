class Subassemblage < ActiveRecord::Base
	has_many :element
	belongs_to :assemblage

	before_save :set_volume, :set_surface

    def elements_list
    	@elements = Element.where("subassemblage_id = ?", self.id)
    end

	def set_volume
		if elements_list.empty?
			self.volume = self.height * self.width * self.depth * self.quantity / 1000000000
		else
			self.volume = BigDecimal.new("0.000")
			elements_list.each do |e|
				self.volume = self.volume + e.volume
			end

		end
	end

	def set_surface
		self.surface = self.height * self.width * self.quantity / 1000000
	end

end
