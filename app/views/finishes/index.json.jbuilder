json.array!(@finishes) do |finish|
  json.extract! finish, :id, :code, :name, :price, :um
  json.url finish_url(finish, format: :json)
end
