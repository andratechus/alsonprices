json.array!(@subassemblages) do |subassemblage|
  json.extract! subassemblage, :id
  json.url subassemblage_url(subassemblage, format: :json)
end
