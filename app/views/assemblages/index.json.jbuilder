json.array!(@assemblages) do |assemblage|
  json.extract! assemblage, :id
  json.url assemblage_url(assemblage, format: :json)
end
