json.array!(@materials) do |material|
  json.extract! material, :id, :type, :string, :price
  json.url material_url(material, format: :json)
end
