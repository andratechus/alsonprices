json.array!(@elements) do |element|
  json.extract! element, :id, :name, :height, :width, :depth, :volume, :surface, :quantity
  json.url element_url(element, format: :json)
end
