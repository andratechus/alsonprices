json.array!(@expenses) do |expense|
  json.extract! expense, :id, :code, :name, :domestic_value, :currency_code, :currency_value
  json.url expense_url(expense, format: :json)
end
