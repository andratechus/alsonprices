$("#question_search").autocomplete({
  source:$('#question_search').data('source'),
  html: true,
  appendTo: "#search_results",
  select: function( event, ui ) {
    window.location=ui.item.value;
    return false;
  },
  focus: function( event, ui ) { },
  open: function( event, ui ) { }
});
