class AddDimensionsToAssemblage < ActiveRecord::Migration
  def change
    add_column :assemblages, :width, :decimal, precision: 8, scale: 3
    add_column :assemblages, :height, :decimal, precision: 8, scale: 3
    add_column :assemblages, :depth, :decimal, precision: 8, scale: 3
    add_column :assemblages, :net_volume, :decimal, precision: 8, scale: 3
    add_column :assemblages, :net_surface, :decimal, precision: 8, scale: 3
    add_column :assemblages, :brut_volume, :decimal, precision: 8, scale: 3
    add_column :assemblages, :brut_surface, :decimal, precision: 8, scale: 3	
  end
end
