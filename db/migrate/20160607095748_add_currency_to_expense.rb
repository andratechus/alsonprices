class AddCurrencyToExpense < ActiveRecord::Migration
  def change
  	add_reference :expenses, :currency, index: true, foreign_key: true
  end
end
