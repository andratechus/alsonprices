class AddPriceToAccesory < ActiveRecord::Migration
  def change
  	add_column :accesories, :price, :decimal, precision: 8, scale: 2
  	add_column :accesories, :currency_code, :string
  end
end
