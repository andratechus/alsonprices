class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :code
      t.string :name
      t.decimal :price, precision: 7, scale: 2

      t.timestamps null: false
    end
  end
end
