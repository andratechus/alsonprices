class AddAddressLinesToClient < ActiveRecord::Migration
  def change
    add_column :clients, :address_line1, :string
    add_column :clients, :address_line2, :string
  end
end
