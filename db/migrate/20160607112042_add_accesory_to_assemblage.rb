class AddAccesoryToAssemblage < ActiveRecord::Migration
  def change
  	add_reference :assemblages, :accesory, index: true, foreign_key: true
  end
end
