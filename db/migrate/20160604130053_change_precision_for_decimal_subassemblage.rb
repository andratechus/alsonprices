class ChangePrecisionForDecimalSubassemblage < ActiveRecord::Migration
  
  	def self.up
   change_column :subassemblages, :volume, :decimal, :precision => 8, :scale => 3
   change_column :subassemblages, :surface, :decimal, :precision => 8, :scale => 3
  end
end
