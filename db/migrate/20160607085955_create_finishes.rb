class CreateFinishes < ActiveRecord::Migration
  def change
    create_table :finishes do |t|
      t.string :code
      t.string :name
      t.decimal :price, precision: 8, scale: 2
      t.string :um

      t.timestamps null: false
    end
  end
end
