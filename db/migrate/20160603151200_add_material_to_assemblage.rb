class AddMaterialToAssemblage < ActiveRecord::Migration
  def change
    add_reference :assemblages, :material, index: true, foreign_key: true
  end
end
