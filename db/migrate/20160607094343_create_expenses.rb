class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :code
      t.string :name
      t.decimal :domestic_value, precision: 5, scale: 2
      t.string :currency_code
      t.decimal :currency_value, precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end
