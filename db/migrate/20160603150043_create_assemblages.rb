class CreateAssemblages < ActiveRecord::Migration
  def change
    create_table :assemblages do |t|
      t.string :code
      t.string :name
      
      t.timestamps null: false
    end
  end
end
