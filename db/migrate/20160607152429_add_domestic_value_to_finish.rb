class AddDomesticValueToFinish < ActiveRecord::Migration
  def change
  	add_column :finishes, :domestic_price, :decimal, precision: 8, scale: 3
  	add_column :finishes, :currency_code, :string
  	add_reference :finishes, :currency, index: true, foreign_key: true
  end
end
