class AddFinishToAssemblage < ActiveRecord::Migration
  def change
  	add_reference :assemblages, :finish, index: true, foreign_key: true
  end
end
