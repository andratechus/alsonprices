  class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
      t.string :name
      t.decimal :height, precision: 8, scale: 3
      t.decimal :width, precision: 8, scale: 3
      t.decimal :depth, precision: 8, scale: 3
      t.decimal :volume, precision: 8, scale: 3
      t.decimal :surface, precision: 8, scale: 3
      t.belongs_to :subassemblage
      t.integer :quantity
      t.timestamps null: false
    end
  end
end
