class ChangePrecisionForDecimalAssemblage < ActiveRecord::Migration
 
  	def self.up
	   change_column :assemblages, :net_volume, :decimal, :precision => 8, :scale => 3
	   change_column :assemblages, :net_surface, :decimal, :precision => 8, :scale => 3
	   change_column :assemblages, :brut_volume, :decimal, :precision => 8, :scale => 3
	   change_column :assemblages, :brut_surface, :decimal, :precision => 8, :scale => 3
    end
  
end
