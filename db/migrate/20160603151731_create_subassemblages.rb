class CreateSubassemblages < ActiveRecord::Migration
  def change
    create_table :subassemblages do |t|
      t.string :name
      t.string :code
      t.decimal :width, precision: 8, scale: 3
      t.decimal :height, precision: 8, scale: 3
      t.decimal :depth, precision: 8, scale: 3
      t.integer :quantity
      t.decimal :volume, precision: 8, scale: 3
      t.decimal :surface, precision: 8, scale: 3
      t.timestamps null: false
      t.belongs_to :assemblage
    end
  end
end
