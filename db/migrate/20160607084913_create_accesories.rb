class CreateAccesories < ActiveRecord::Migration
  def change
    create_table :accesories do |t|
      t.string :code
      t.string :name
      t.string :um

      t.timestamps null: false
    end
  end
end
