class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :code
      t.string :name
      t.decimal :rate, precision: 5, scale: 3

      t.timestamps null: false
    end
  end
end
