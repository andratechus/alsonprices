class AddOutputCoefficientToMaterial < ActiveRecord::Migration
  def change
    add_column :materials, :output_coefficient, :decimal, precision: 4, scale: 2
  end
end
