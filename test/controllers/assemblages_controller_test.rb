require 'test_helper'

class AssemblagesControllerTest < ActionController::TestCase
  setup do
    @assemblage = assemblages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:assemblages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create assemblage" do
    assert_difference('Assemblage.count') do
      post :create, assemblage: {  }
    end

    assert_redirected_to assemblage_path(assigns(:assemblage))
  end

  test "should show assemblage" do
    get :show, id: @assemblage
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @assemblage
    assert_response :success
  end

  test "should update assemblage" do
    patch :update, id: @assemblage, assemblage: {  }
    assert_redirected_to assemblage_path(assigns(:assemblage))
  end

  test "should destroy assemblage" do
    assert_difference('Assemblage.count', -1) do
      delete :destroy, id: @assemblage
    end

    assert_redirected_to assemblages_path
  end
end
