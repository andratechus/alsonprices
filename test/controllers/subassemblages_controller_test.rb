require 'test_helper'

class SubassemblagesControllerTest < ActionController::TestCase
  setup do
    @subassemblage = subassemblages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:subassemblages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create subassemblage" do
    assert_difference('Subassemblage.count') do
      post :create, subassemblage: {  }
    end

    assert_redirected_to subassemblage_path(assigns(:subassemblage))
  end

  test "should show subassemblage" do
    get :show, id: @subassemblage
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @subassemblage
    assert_response :success
  end

  test "should update subassemblage" do
    patch :update, id: @subassemblage, subassemblage: {  }
    assert_redirected_to subassemblage_path(assigns(:subassemblage))
  end

  test "should destroy subassemblage" do
    assert_difference('Subassemblage.count', -1) do
      delete :destroy, id: @subassemblage
    end

    assert_redirected_to subassemblages_path
  end
end
