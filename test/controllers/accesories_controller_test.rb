require 'test_helper'

class AccesoriesControllerTest < ActionController::TestCase
  setup do
    @accesory = accesories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:accesories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create accesory" do
    assert_difference('Accesory.count') do
      post :create, accesory: { code: @accesory.code, name: @accesory.name, um: @accesory.um }
    end

    assert_redirected_to accesory_path(assigns(:accesory))
  end

  test "should show accesory" do
    get :show, id: @accesory
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @accesory
    assert_response :success
  end

  test "should update accesory" do
    patch :update, id: @accesory, accesory: { code: @accesory.code, name: @accesory.name, um: @accesory.um }
    assert_redirected_to accesory_path(assigns(:accesory))
  end

  test "should destroy accesory" do
    assert_difference('Accesory.count', -1) do
      delete :destroy, id: @accesory
    end

    assert_redirected_to accesories_path
  end
end
